This is based on code from Mirco Deckenhoff and modified by Blake Leverington, Peter Stromberger, _et al_.
### Based on https://gitlab.cern.ch/lhcb-scifi/SciFiSimG4, but now expanded to a fibre mat, rather than a single fibre, with SiPM readout.


## How to get SciFiSim running
 _Installation of GEANT4 help is at the bottom of this readme. Check the cmake options for compatibility._


 > $ git clone https://$USER@gitlab.cern.ch/lhcb-scifi/SciFiMatG4_v2
 
 > $ cd SciFiMatG4_v2
 
Edit the environment script setup.sh to point to your Geant and ROOT installation and `source` it to add it to your environment:

 > $ source setup.sh //(required before running the software too if you open a new terminal.)

Now to cun cmake in the build folder:

 >  mkdir -p SciFiSim-build //it is safe to delete this folder if you have problems during compilation

 >  cd SciFiSim-build

 >  cmake ../SciFiSim
 
 >  cmake --build . // or make clean; make


To run the Geant4 simulation in batch mode:
 
 >  cd ../SimulationData
 
 >  ../SciFiSim-build/scifiSim muongun.mac


To run the simulation from within the GUI (for visualization only) very slow if optical photon physics are enabled:
 > $ ../SciFiSim-build/scifiSim //opens the gui
 > /control/execute/ vis.mac



## The Geometry file
../SciFiSim/src/DetectorConstruction.cc&

 - a multi-layer ribbon of fibres with LHCb style SiPM array at the end (no 64ch gap)

 *if you make changes to the code, run `make` again in the build folder to recompile with the changes. Sometimes you might need `make clean; make`.*

## Input Parameters
Various parameters of the simulation are read in from a parameters file in /SimulationData/parameterFiles/ .
The description is in SciFiSi 

## PhysicsList.cc
The optical photon physics can be turned off by commenting out the OpticalPhyics section with `/*  */`  in /src/PhysicsList.cc
if you are only interested in energy deposit in the fibres from the primary track, for example. 


## The root output

There are 5 different TTrees created.

 - DetectedPhotons (information about photons detected at SiPM, each photon is an entry)
 - Trigger(information about trigger)
 - PrimaryParticleTrack
 - InitialParticle(Not sure what the difference is)
 - EnergyTrack (track length in fibres and energy deposited)
 
## SciFiSimG4_v2 requires installation of Geant4 first
 
 > $ wget http://geant4.web.cern.ch/geant4/support/source/geant4.10.07.p01.tar.gz
 
 > $ tar -xzvf geant4.10.07.p01.tar.gz 
 
 > $ mkdir geant4.10.07.p01-build
 
 > $ mkdir geant4.10.07.p01-install
 
 > $ cd geant4.10.07.p01-build/
 


 > https://geant4-userdoc.web.cern.ch/UsersGuides/InstallationGuide/html/gettingstarted.html#softwarerequirements
 
 > sudo apt-get install libxerces-c-dev qt4-dev-tools freeglut3-dev libmotif-dev tk-dev cmake libxpm-dev libxmu-dev libxi-dev
 > libqt5opengl5-dev
 >libqt4-dev
 >cmake libx11-dev xorg-dev libglu1-mesa-dev freeglut3-dev libglew1.5 libglew1.5-dev libglu1-mesa libglu1-mesa-dev libgl1-mesa-glx libgl1-mesa-dev
 >libfontconfig1-dev libfreetype6-dev libx11-dev libxcursor-dev libxext-dev libxfixes-dev libxft-dev libxi-dev libxrandr-dev libxrender-dev
 >libxmu-dev expat
 >freeglut3 freeglut3-dev mesa-utils

### add your favourite options. the Granular libraries are required by the code, for some reason.

 > cmake -DCMAKE_INSTALL_PREFIX=/home/leverington/software/geant4.10.07.p01-install -DGEANT4_INSTALL_DATA=ON -DGEANT4_BUILD_MULTITHREADED=ON -DGEANT4_USE_OPENGL_X11=ON -DGEANT4_USE_QT=ON -DGEANT4_BUILD_CXXSTD=17 -DGEANT4_USE_FREETYPE=ON -DGEANT4_USE_SYSTEM_CLHEP_GRANULAR=ON ../geant4.10.07.p01

 > fix any missing software that is might complain is missing

 > $ make
 
 > $ make install


## Cern ROOT must also be installed. Compiled with CXX17
currently works with ROOT Version: 6.23/01

> $ root-config --features

 > cxx17 asimage builtin_afterimage builtin_clang builtin_gl2ps builtin_llvm builtin_lz4 builtin_nlohmannjson builtin_openui5 builtin_tbb builtin_vdt builtin_xrootd builtin_xxhash builtin_zstd clad dataframe exceptions fftw3 fitsio gdml http imt mathmore mlp mysql opengl pyroot roofit webgui root7 runtime_cxxmodules shared ssl tmva tmva-cpu tmva-pymva spectrum vdt x11 xml xrootd

 > cmake -Dpython=ON -Droofit=ON -DCMAKE_INSTALL_PREFIX:PATH=/home/leverington/software/root-install-cxx17 -DCMAKE_CXX_STANDARD=17 ../root

 > make

 > make install
