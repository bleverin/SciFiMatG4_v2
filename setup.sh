source /home/leverington/software/geant4.10.07.p01-install/share/Geant4-10.7.1/geant4make/geant4make.sh
source /home/leverington/software/root-install-cxx17/bin/thisroot.sh
echo 'G4INSTALL='$G4INSTALL
echo 'ROOTSYS='$ROOTSYS
export GEANT4_DIR=$G4INSTALL/lib
export G4DATA=/home/leverington/software/geant4.10.07.p01-install/share/Geant4-10.7.1/data
export G4LEDATA=${G4DATA}/G4EMLOW7.13
export G4LEVELGAMMADATA=${G4DATA}/PhotonEvaporation5.7
export G4NEUTRONHPDATA=${G4DATA}/G4NDL4.6
export G4RADIOACTIVEDATA=${G4DATA}/RadioactiveDecay5.6
export G4ABLADATA=${G4DATA}/G4ABLA3.1
export G4REALSURFACEDATA=${G4DATA}/RealSurface2.2
export G4PIIDATA=${G4DATA}/G4PII1.3
export G4SAIDXSDATA=${G4DATA}/G4SAIDDATA2.0
export CLHEP_DIR=/home/leverington/software/geant4.10.07.p01/source/externals/clhep
export BOOST_ROOT=/usr/include/boost/
export CXX=/usr/bin/g++
export CC=/usr/bin/gcc
#export ROOTSYS=$ROOTSYS

printenv

