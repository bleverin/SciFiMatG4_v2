// Written by Peter Stromberger
// based on work of Mirco Deckenhoff
// modified by Bastian Rössler

#include "ActionInitialization.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"
#include "EventAction.hh"

#include "StackingAction.hh"
#include "SteppingAction.hh"


// constructor
ActionInitialization::ActionInitialization() : G4VUserActionInitialization() {}

// destructor
ActionInitialization::~ActionInitialization() {}

// methods

void ActionInitialization::BuildForMaster() const
{
    SetUserAction( new RunAction);
}

void ActionInitialization::Build() const
{
	PrimaryGeneratorAction* pGA = new PrimaryGeneratorAction;
	SetUserAction( pGA);
	SetUserAction( new RunAction);
	EventAction* eventAction = new EventAction;
	SetUserAction( eventAction);
	SetUserAction( new StackingAction);
	SetUserAction( new SteppingAction(eventAction, pGA));
}

