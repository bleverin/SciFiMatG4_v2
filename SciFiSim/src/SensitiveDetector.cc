// Written by Peter Stromberger
// based on work of Mirco Deckenhoff
// modified by Bastian Rössler


#include "SensitiveDetector.hh"
#include "G4TouchableHistory.hh"
#include "G4Step.hh"
#include "G4HCofThisEvent.hh"
#include "G4HCtable.hh"
#include "G4UnitsTable.hh"
#include "G4VProcess.hh"
#include "G4ParticleTypes.hh"
#include "Analysis.hh"
#include "G4UIcommand.hh"
#include "G4EventManager.hh"
#include "G4RunManager.hh"



SensitiveDetector::SensitiveDetector(G4String SDname): G4VSensitiveDetector(SDname){}

SensitiveDetector::~SensitiveDetector(){}

G4bool SensitiveDetector::ProcessHits(G4Step * step, G4TouchableHistory *)
{
    G4TouchableHandle touchable = step->GetPreStepPoint()->GetTouchableHandle();

    /* ++ particle is at geometry boundary and an optical photon (for SiPM) ++ */
    if(step->GetPreStepPoint()->GetStepStatus() == fGeomBoundary
       && step->GetTrack()->GetDefinition() == G4OpticalPhoton::OpticalPhotonDefinition())
    {
        if(step->GetStepLength() != 0) // Avoid division by zero and counting reflected photons
        {
            G4int creatorProcessId = 0;

            if(step->GetTrack()->GetCreatorProcess()->GetProcessName() == "Scintillation")
                creatorProcessId = 1;

            if (step->GetTrack()->GetCreatorProcess()->GetProcessName() == "Cerenkov")
                creatorProcessId = 2;

            if (step->GetTrack()->GetCreatorProcess()->GetProcessName() == "OpWLS")
                creatorProcessId = 3;
	
            G4double myRunID = (G4double) G4RunManager::GetRunManager()->GetCurrentRun()->GetRunID();

            G4double myEventID = (G4double) G4EventManager::GetEventManager()->GetConstCurrentEvent()->GetEventID();
      				
            G4double myDetectorID = G4UIcommand::ConvertToDouble((G4String)
                                                                 (step->GetPreStepPoint()->GetPhysicalVolume()->GetName()).substr(17,3));
			
            G4double xPixel = G4UIcommand::ConvertToDouble((G4String)
                                                           (step->GetPreStepPoint()->GetPhysicalVolume()->GetName()).substr(20,1));

            G4double yPixel = G4UIcommand::ConvertToDouble((G4String)
                                                           (step->GetPreStepPoint()->GetPhysicalVolume()->GetName()).substr(21,2));
				
            Analysis::GetInstance()->FillDetectedPhotons(myRunID,
                                                         myEventID,
                                                         myDetectorID,
                                                         xPixel,
                                                         yPixel,
                                                         step->GetTrack()->GetTotalEnergy()*1e6,
                                                         step->GetTrack()->GetLocalTime()-step->GetDeltaTime(),
                                                         step->GetTrack()->GetTrackLength()-step->GetStepLength(),
                                                         step->GetTrack()->GetGlobalTime()-step->GetDeltaTime(),
                                                         step->GetTrack()->GetPosition()[0]-step->GetDeltaPosition()[0],
                                                         step->GetTrack()->GetPosition()[1]-step->GetDeltaPosition()[1],
                                                         step->GetTrack()->GetPosition()[2]-step->GetDeltaPosition()[2],
                                                         step->GetDeltaPosition()[0]/step->GetStepLength(),
                                                         step->GetDeltaPosition()[1]/step->GetStepLength(),
                                                         step->GetDeltaPosition()[2]/step->GetStepLength(),
                                                         step->GetTrack()->GetVertexPosition()[0],
                                                         step->GetTrack()->GetVertexPosition()[1],
                                                         step->GetTrack()->GetVertexPosition()[2],
                                                         step->GetTrack()->GetVertexMomentumDirection()[0],
                                                         step->GetTrack()->GetVertexMomentumDirection()[1],
                                                         step->GetTrack()->GetVertexMomentumDirection()[2],
                                                         step->GetTrack()->GetTrackID(),
                                                         creatorProcessId,
                                                         step->GetTrack()->GetParentID());
        }
		
        // Also kill reflected photons to avoid many reflections without detection
        step->GetTrack()->SetTrackStatus(fKillTrackAndSecondaries);
    }
  
    return true;
}
