// Written by Mirco DECKENHOFF
// based on
// $Id: RunAction.cc,v 1.10 2006/06/29 17:54:31 gunter Exp $
// GEANT4 tag $Name: geant4-09-01-ref-02 $
//
// modified by Bastian Rössler

#include "G4Timer.hh"

#include "RunAction.hh"

#include "G4Run.hh"

#include "Analysis.hh"


RunAction::RunAction()
{
    timer = new G4Timer;
}


RunAction::~RunAction()
{
    delete timer;
}


void RunAction::BeginOfRunAction(const G4Run* aRun)
{
    G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
    timer->Start();
    Analysis::GetInstance()->PrepareNewRun(aRun);
    G4cout << "Analysis Instance Address: " << Analysis::GetInstance() << G4endl;
}


void RunAction::EndOfRunAction(const G4Run* aRun)
{   
    timer->Stop();
    G4cout << "number of event = " << aRun->GetNumberOfEvent() << " \t" << *timer << G4endl;
    Analysis::GetInstance()->EndOfRun(/*aRun*/);
}