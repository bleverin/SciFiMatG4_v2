// Written by Mirco DECKENHOFF based on
//
// $Id: PhysicsList.cc,v 1.17 2009/11/10 05:16:23 gum Exp $
//
//
// modified by Peter Stromberger
// modified by Bastian Rössler

#include "PhysicsList.hh"

#include "G4EmStandardPhysics.hh"
#include "G4DecayPhysics.hh"
#include "G4OpticalPhysics.hh"
#include "G4ProcessManager.hh"
#include "G4ParticleTypes.hh"
#include "G4hIonisation.hh"

#include "Parameters.hh"

#include "G4SystemOfUnits.hh"

PhysicsList::PhysicsList() :  G4VModularPhysicsList()
{
    // default cut value  (1.0mm)
    defaultCutValue = 1.0*mm;

    // * EM Physics
    RegisterPhysics( new G4EmStandardPhysics() );

    
    // * Optical Physics
    G4OpticalPhysics* opticalPhysics = new G4OpticalPhysics();
    RegisterPhysics( opticalPhysics );

    // adjust some parameters for the optical physics
    opticalPhysics->SetWLSTimeProfile("exponential");

    opticalPhysics->SetScintillationYieldFactor(1.0);
    opticalPhysics->SetScintillationExcitationRatio(Parameters::GetInstance()->YieldRatio());

    opticalPhysics->SetMaxNumPhotonsPerStep(100);
    opticalPhysics->SetMaxBetaChangePerStep(10.0);
    
    // opticalPhysics->SetTrackSecondariesFirst(true);
       
}



void PhysicsList::ConstructParticle()
{
    // Constructs all paricles
    G4VModularPhysicsList::ConstructParticle();
}



void PhysicsList::SetCuts()
{
    //  " G4VUserPhysicsList::SetCutsWithDefault" method sets
    //   the default cut value for all particle types
    //
    SetCutsWithDefault();
  
    if (verboseLevel>0)
        DumpCutValuesTable();
}

