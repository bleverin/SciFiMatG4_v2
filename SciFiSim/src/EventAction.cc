// Written by Peter Stromberger
// based on work of Mirco Deckenhoff
// modified by Bastian Rössler


#include "EventAction.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4ios.hh"

#include "Analysis.hh"
#include "G4RunManager.hh"
 
EventAction::EventAction() : fEnergy(0.), fTrackL(0.)
{}

 
EventAction::~EventAction()
{}

 
void EventAction::BeginOfEventAction(const G4Event* anEvent)
{
    G4cout << "# Event " << anEvent->GetEventID() << " start." << G4endl;
    Analysis::GetInstance()->PrepareNewEvent(anEvent);

    fEnergy = 0.0;
    fTrackL = 0.0;
}

 
void EventAction::EndOfEventAction(const G4Event* anEvent)
{
    G4double runID = (G4double) G4RunManager::GetRunManager()->GetCurrentRun()->GetRunID();
    Analysis::GetInstance()->FillEnergyTrack(runID,	anEvent->GetEventID(), fEnergy, fTrackL);
}
