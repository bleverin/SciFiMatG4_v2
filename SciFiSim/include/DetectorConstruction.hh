// Written by Peter Stromberger
// based on work of Mirco Deckenhoff

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4ThreeVector.hh"


class G4Element;
class G4Material;
class G4LogicalVolume;
class G4MaterialPropertiesTable;

class DetectorConstruction : public G4VUserDetectorConstruction
{
public:
    DetectorConstruction();
    ~DetectorConstruction();
   
    G4VPhysicalVolume* Construct();

  G4LogicalVolume* GetScoringVolume() const { return fScoringVolume; }


 protected:
    G4LogicalVolume*  fScoringVolume;

private:

    /* ++ methods ++ */
    void DefineMaterials();
    void DefineMaterialProperties();
    void ConstructFiber(); // Constructs and places the fibres
  void ConstructFiberSheet(); // Constructs and places the fibres
    G4ThreeVector objectPos(G4int, G4int); // for positioning of multiple fibres
    G4ThreeVector objectPos(G4int, G4int, G4double);// for positioning of detector
    G4ThreeVector objectPos(G4int, G4int, G4int, G4int, G4double); // pixel positioning

    /* ++ attributes ++ */

    // geometry
    G4double detector_x, detector_y, detector_z;
    G4double scint_x, scint_y, scint_z;
    G4int Nj, Nk;
    G4double xDist, yDist;
    G4double stripWidth, stripHeight;
    G4double airGap, pixelDimX, pixelDimY, epoxy_strip_width;
    int Nx, Ny;
  G4bool fMirrorToggle;
  G4double fMirrorPolish, fMirrorReflectivity, fMirrorZ, fMirrorRmax;
  G4double fWorldSizeX, fWorldSizeY,fWorldSizeZ;
    // materials
    G4Material *Vacuum;
    G4Material *Air;
    G4Material *Pstyrene;
    G4Material *PMMA;
    G4Material *PMMA2;
    G4Material *Epoxy;
    G4Material *Glue;
    G4Material *TiO2;
    G4Material *Abs_plastic;
    G4Material *alu;

    // material properties
    G4MaterialPropertiesTable* MPTPStyrene;

    //world
    G4LogicalVolume * fLogicWorld;
    G4VPhysicalVolume * fPhysiWorld;

    // detector assembly
    G4LogicalVolume * airbox_log;
    G4VPhysicalVolume * airbox_phys;
    
    // detector assembly
    G4LogicalVolume * detector_log;
    G4VPhysicalVolume * detector_phys;
    
  
    // epoxybox
    G4LogicalVolume* epoxyLog;
    G4VPhysicalVolume* epoxyPhy;

    // abs plastic
    G4LogicalVolume* absLog;
    G4VPhysicalVolume* absPhy;


    // materials and properties for fibre sections
    G4Material* scintCoreMaterial;
    G4Material* innerCladdingMaterial;
    G4Material* outerCladdingMaterial;


};

#endif
