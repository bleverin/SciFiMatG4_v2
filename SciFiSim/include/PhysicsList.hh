// $Id: PhysicsList.hh,v 1.7 2006/06/29 17:53:59 gunter Exp $
//

#ifndef PhysicsList_h
#define PhysicsList_h 1

#include "globals.hh"
#include "G4VModularPhysicsList.hh"


class PhysicsList : public G4VModularPhysicsList
{
public:
    PhysicsList();

public:
    void ConstructParticle();
    void SetCuts();

};


#endif /* PhysicsList_h */

