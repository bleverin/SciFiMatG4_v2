// Written by Peter Stromberger
// based on work of Mirco Deckenhoff

#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1


#include "globals.hh"
#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4GeneralParticleSource.hh"

class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
public:
    PrimaryGeneratorAction();
    ~PrimaryGeneratorAction();

    void GeneratePrimaries(G4Event*);
    void SetOptPhotonPolar();
    void SetOptPhotonPolar(G4double);
    G4GeneralParticleSource* GetGun();

private:  
    G4GeneralParticleSource * InitializeGPS();
    G4GeneralParticleSource * gun;
};


#endif
