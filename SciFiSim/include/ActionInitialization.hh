/*
 *	Author	: Peter Stromberger
 *	Email	: stromberger@physi.uni-heidelberg.de
 *	Date	: 14.05.2014
 *
 *	Description: Definition of the ActionInitialization class
 *
 *	file: ActionInitialization.hh
 */

#ifndef ActionInitialization_h
#define ActionInitialization_h 1

#include "G4VUserActionInitialization.hh"


class ActionInitialization : public G4VUserActionInitialization{

public:
    ActionInitialization();
    virtual ~ActionInitialization();

    virtual void BuildForMaster() const;
    virtual void Build() const;
};

#endif
