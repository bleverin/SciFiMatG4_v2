// Written by Peter Stromberger
// based on work of Mirco Deckenhoff

#ifndef StackingAction_H
#define StackingAction_H 1

#include "globals.hh"
#include "G4UserStackingAction.hh"

#include <map>

class G4VProcess;

class StackingAction : public G4UserStackingAction
{
public:
    StackingAction();
    ~StackingAction();

public:
    G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track* aTrack);
    void NewStage();
    void PrepareNewEvent();

private:
    G4int gammaCounter;
    typedef std::map<const G4VProcess*,G4int> ProcMap;
    ProcMap procs;
    void AddProcess(const G4VProcess*);
    void Reset();
};


#endif

