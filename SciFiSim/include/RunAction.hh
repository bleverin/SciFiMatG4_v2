//
// $Id: RunAction.hh,v 1.9 2006/06/29 17:54:10 gunter Exp $
// GEANT4 tag $Name: geant4-09-01-ref-02 $

#ifndef RunAction_h
#define RunAction_h 1

#include "globals.hh"
#include "G4UserRunAction.hh"


class G4Timer;
class G4Run;

class RunAction : public G4UserRunAction
{
public:
    RunAction();
    ~RunAction();

public:
    void BeginOfRunAction(const G4Run* aRun);
    void EndOfRunAction(const G4Run* aRun);

private:
    G4Timer* timer;
};


#endif /*RunAction_h*/
