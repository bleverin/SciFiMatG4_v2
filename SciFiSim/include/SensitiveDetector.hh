// Written by Peter Stromberger
// based on work of Mirco Deckenhoff

#ifndef SensitiveDetector_h
#define SensitiveDetector_h 1

#include "G4VSensitiveDetector.hh"

class G4Step;
class G4TouchableHistory;
class G4HCofThisEvent;

class SensitiveDetector : public G4VSensitiveDetector
{
public:
    SensitiveDetector(G4String SDname);
    ~SensitiveDetector();

    G4bool ProcessHits(G4Step * step, G4TouchableHistory * ROhist);
};

#endif

