// Written by Peter Stromberger

#ifndef CONVERT_h
#define CONVERT_h 1

#include "G4UIcommand.hh"
#include "globals.hh"

class C
{
public:

    static G4String c(G4int);
    static G4String c1(G4int);
    static G4String c2(G4int);
    static G4String c3(G4int);
};


inline G4String C::c(G4int i)
{
    return G4UIcommand::ConvertToString(i);
}


inline G4String C::c1(G4int i)
{
    G4String r;
    if(i <10)
        r = "00";
    if(i > 9 && i < 100)
        r = "0";
    if(i > 99)
        r = "";

    return r+G4UIcommand::ConvertToString(i);
}

inline G4String C::c2(G4int i)
{
    return G4UIcommand::ConvertToString(i);
}

inline G4String C::c3(G4int i)
{
    G4String r;

    if( i< 10)
        r = "0";
    if(i > 9)
        r = "";
		
    return r+G4UIcommand::ConvertToString(i);
}

#endif

