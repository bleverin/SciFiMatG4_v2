// Written by Peter Stromberger
// based on work of Mirco Deckenhoff
 
#ifndef EventAction_h
#define EventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"

class G4Event;


class EventAction : public G4UserEventAction
{
public:
    EventAction();
    ~EventAction();

public:
    void BeginOfEventAction(const G4Event* anEvent);
    void EndOfEventAction(const G4Event*);

    void AddCore(G4double de, G4double dl);

private:
    G4double fEnergy;
    G4double fTrackL;

};


inline void EventAction::AddCore(G4double de, G4double dl)
{
    fEnergy += de;
    fTrackL += dl;
}

#endif

    
