// Written by Peter Stromberger
// based on work of Mirco Deckenhoff

#ifndef SteppingAction_H
#define SteppingAction_H 1

 
#include "globals.hh"
#include "G4UserSteppingAction.hh"
#include "G4OpBoundaryProcess.hh"
#include "EventAction.hh"
#include "G4ParticleDefinition.hh"
#include "PrimaryGeneratorAction.hh"
#include "TFile.h"
#include "TH2F.h"
#include "TH1F.h"
#include "TTree.h"
#include "TObject.h"




class SteppingAction : public G4UserSteppingAction
{
  


public:
    SteppingAction(EventAction* eventAction, PrimaryGeneratorAction* pGA);
    virtual ~SteppingAction();
    virtual void UserSteppingAction( const G4Step* );


private:
    const char* primParticle;
    G4ParticleDefinition* primDefinition;
    PrimaryGeneratorAction* pGA;
    EventAction* fEventAction;
    G4int creatorProcess;
    G4OpBoundaryProcess* opBoundaryProcess;


  
 


};



#endif
