// Written by Peter Stromberger
// based on work of Mirco Deckenhoff


#ifndef PARAMETERS_HH_
#define PARAMETERS_HH_ 1

#include "G4Event.hh"
#include "G4Run.hh"

#include "TFile.h"
#include "TH2F.h"
#include "TH1F.h"
#include "TTree.h"
#include "TObject.h"




class Parameters
{

public:
  
    static Parameters* GetInstance()
    {
        if ( Parameters::singleton == NULL ) Parameters::singleton = new Parameters();
        return Parameters::singleton;
    }

    virtual ~Parameters();


    char* ParameterOutputFileName();

    G4double RandomSeed();
    G4double RandomNumber();
    G4double FibreLength();
    G4double SemiAxisZ();
    G4double SemiAxisY();
    G4double ProbabilityOfPhotonLossAtSurface();
    G4bool PlaceMirror();
    G4double MirrorReflectivity();
    G4bool DetectorMaterial();
  
    G4double TriggerX();
    G4double TriggerY();
    G4double TriggerZ();
  
    G4double TriggerXPos();
    G4double TriggerZPos();

    char* EmissionSpectrumFileName();
    G4int NumberOfEnergies();
    G4double* Energy;
    G4double* Intensity;
    G4int NumberOfInterpolatedPoints();

    char* WlsAbsSpectrumFileName();
    G4int NumberOfWlsAbsEnergies();
    G4double* WlsAbsEnergy;
    G4double* WlsAbsLength;

    char* WlsEmissionSpectrumFileName();
    G4int NumberOfWlsEmissionEnergies();
    G4double* WlsEmissionEnergy;
    G4double* WlsEmissionIntensity;

    G4double ScintillationYield();
    G4double ResolutionScale();
    G4double DecayTimeFast();
    G4double DecayTimeSlow();
    G4double YieldRatio();
    G4double BirksConstant();

    G4double WlsDecayTime();

    char* RefractiveIndexVacuum();
    char* RefractiveIndexCore();
    char* RefractiveIndexClad1();
    char* RefractiveIndexClad2();

    char* AbsorptionCore();
    char* AbsorptionClad1();
    char* AbsorptionClad2();
 
    char* AbsorptionFromIrradiationCore();
    char* AbsorptionFromIrradiationClad1();
    char* AbsorptionFromIrradiationClad2();

    char* SectionsFileName();
    G4int NumberOfSections();

    char* RayleighCore();
    char* RayleighClad1();
    char* RayleighClad2();

    // Conversion factor between wavelength and energy
    const static G4double hcPERe;


private:

    Parameters();
    static Parameters* singleton;


    char parameterOutputFileName[50];

    G4double randomSeed;
    G4double randomNumber;
    G4double fibreLength;
    G4double semiAxisZ;
    G4double semiAxisY;
    G4double probabilityOfPhotonLossAtSurface;
    G4bool placeMirror;
    G4double mirrorReflectivity;
    G4bool detectorMaterial;
  
    G4double triggerX;
    G4double triggerY;
    G4double triggerZ;
  
    G4double triggerXPos;
    G4double triggerZPos;

    char emissionSpectrumFileName[256];
    G4int numberOfEnergies;
    //  G4double[] energy;
    //  G4double[] intensity;
    G4int numberOfInterpolatedPoints;

    char wlsAbsSpectrumFileName[256];
    G4int numberOfWlsAbsEnergies;

    char wlsEmissionSpectrumFileName[256];
    G4int numberOfWlsEmissionEnergies;

    G4double scintillationYield;
    G4double resolutionScale;
    G4double decayTimeFast;
    G4double decayTimeSlow;
    G4double yieldRatio;
    G4double birksConstant;

    G4double wlsDecayTime;

    char refractiveIndexVacuum[256];
    char refractiveIndexCore[256];
    char refractiveIndexClad1[256];
    char refractiveIndexClad2[256];

    char absorptionCore[512];
    char absorptionClad1[512];
    char absorptionClad2[512];
 
    char absorptionFromIrradiationCore[512];
    char absorptionFromIrradiationClad1[512];
    char absorptionFromIrradiationClad2[512];

    char sectionsFileName[80];
    G4int numberOfSections;

    char rayleighCore[512];
    char rayleighClad1[512];
    char rayleighClad2[512];

};

#endif
